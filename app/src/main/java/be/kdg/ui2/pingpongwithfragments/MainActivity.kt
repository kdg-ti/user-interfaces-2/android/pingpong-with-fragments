package be.kdg.ui2.pingpongwithfragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.commit
import be.kdg.ui2.pingpongwithfragments.fragments.PingFragment
import be.kdg.ui2.pingpongwithfragments.fragments.PongFragment

class MainActivity : AppCompatActivity(), PongFragment.PongClickListener, PingFragment.PingClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val pingFragment = PingFragment()
        supportFragmentManager.commit {
            add(R.id.clFragmentContainer, pingFragment)
        }
    }

    override fun onPongClick() {
        //switch to Ping fragment
        val pingFragment = PingFragment()
        supportFragmentManager.commit {
            replace(R.id.clFragmentContainer, pingFragment)
        }
    }

    override fun onPingClick() {
        //switch to Pong fragment
        val pongFragment = PongFragment()
        supportFragmentManager.commit {
            replace(R.id.clFragmentContainer, pongFragment)
        }
    }
}

