package be.kdg.ui2.pingpongwithfragments.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import be.kdg.ui2.pingpongwithfragments.R

/**
 * A simple [Fragment] subclass.
 */
class PingFragment : Fragment() {
    private var listener: PingClickListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PingClickListener) {
            listener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ping, container, false)
        view.findViewById<ImageView>(R.id.pingImageView).setOnClickListener {
            listener?.onPingClick()
        }
        return view
    }

    interface PingClickListener {
        fun onPingClick()
    }
}
