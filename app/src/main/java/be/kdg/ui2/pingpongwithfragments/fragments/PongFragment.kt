package be.kdg.ui2.pingpongwithfragments.fragments


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import be.kdg.ui2.pingpongwithfragments.R

/**
 * A simple [Fragment] subclass.
 */
class PongFragment : Fragment() {
    private var listener: PongClickListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PongClickListener) {
            listener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pong, container, false)
        view.findViewById<ImageView>(R.id.pongImageView).setOnClickListener {
            listener?.onPongClick()
        }
        return view
    }

    interface PongClickListener {
        fun onPongClick()
    }
}
